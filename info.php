<?php
require_once 'db/db.php';
require_once 'classes/Factory.php';
$role = $_POST['role'] ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Заполните форму</h1>
<form action="seeder_db.php" method="post" name="all">
    <ul>
        <select name="role" hidden>
            <option value="<?= $_POST['role'] ?>"></option>
        </select>
        <li>ФИО:<input type="text" name="fullName"></li>
        <br>
        <li>Номер телефона:<input type="tel" name="phoneNumber"></li>
        <br>
        <li>Почтовый ящик:<input type="email" name="email"></li>
        <br>
        <?php if ($role == 'Студент'): ?>
            <li>Успеваемость:<input type="number" name="average"></li>
            <br>
        <?php elseif ($role == 'Преподаватель') : ?>
            <li>Предметы:<input type="text" name="subject"></li>
            <br>
        <?php elseif ($role == 'Администратор') : ?>
            <li>Дни работы:<br>
                <input type="checkbox" name="day1" value="Понедельник">Понедельник<Br>
                <input type="checkbox" name="day2" value="Вторник">Вторник<Br>
                <input type="checkbox" name="day3" value="Среда">Среда<Br>
                <input type="checkbox" name="day4" value="Четверг">Четверг<Br>
                <input type="checkbox" name="day5" value="Пятница">Пятница<Br>
                <input type="checkbox" name="day6" value="Суббота">Суббота<Br>
                <input type="checkbox" name="day7" value="Воскресенье">Воскресенье<Br></li>
            <br>
        <?php endif; ?>
        <button>Отправить</button>
    </ul>
</form>
</body>
</html>
