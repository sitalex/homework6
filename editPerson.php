<?php
if (empty($_GET['id'] && $_GET['role']))
{
    header('Location:index.php');
}

require_once 'db/db.php';
require_once 'classes/Factory.php';
require_once 'classes/Student.php';
require_once 'classes/Teacher.php';
require_once 'classes/Admin.php';

if ($_GET['role'] == 'Студент')
{
    $person = Student::getPerson($_GET['id'], $pdo);
}
else if ($_GET['role'] == 'Преподаватель')
{
    $person = Teacher::getPerson($_GET['id'], $pdo);
}
else if ($_GET['role'] == 'Администратор')
{
    $person = Admin::getPerson($_GET['id'], $pdo);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Заполните форму</h1>
<form action="updatePerson.php" method="post" name="all">
    <ul>
        <select name="role" hidden>
            <option value="<?= $_GET['role'] ?>"></option>
        </select>
        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
        <li>ФИО:<input type="text" name="fullName" value="<?= $person->getFullName() ?>"></li>
        <br>
        <li>Номер телефона:<input type="tel" name="phoneNumber" value="<?= $person->getPhoneNumber() ?>"></li>
        <br>
        <li>Почтовый ящик:<input type="email" name="email" value="<?= $person->getEmail() ?>"></li>
        <br>
        <?php if ($_GET['role'] == 'Студент'): ?>
            <li>Успеваемость:<input type="number" name="average" value="<?= $person->getAverage() ?>"></li>
            <br>
        <?php elseif ($_GET['role'] == 'Преподаватель') : ?>
            <li>Предметы:<input type="text" name="subject" value="<?= $person->getSubject() ?>"></li>
            <br>
        <?php elseif ($_GET['role'] == 'Администратор') : ?>
            <li>Дни работы:<br>
                <input type="checkbox" name="day1" value="Понедельник">Понедельник<Br>
                <input type="checkbox" name="day2" value="Вторник">Вторник<Br>
                <input type="checkbox" name="day3" value="Среда">Среда<Br>
                <input type="checkbox" name="day4" value="Четверг">Четверг<Br>
                <input type="checkbox" name="day5" value="Пятница">Пятница<Br>
                <input type="checkbox" name="day6" value="Суббота">Суббота<Br>
                <input type="checkbox" name="day7" value="Воскресенье">Воскресенье<Br></li>
            <br>
        <?php endif; ?>
        <button>Отправить</button>
    </ul>
</form>
</body>
</html>

