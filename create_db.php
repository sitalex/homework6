<?php
require_once 'db/db.php';

try
{
    $sql = "CREATE TABLE school (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255),
    role VARCHAR(255),
    average_mark Float,
    subject VARCHAR(255),
    working_day VARCHAR(255)) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);
}
catch (Exception $ex)
{
    echo "Error creating table: " . $ex->getCode() . ' ' . $ex->getMessage() . '<br>';
    die('<a href="index.php">Вернуться</a>');
}


header('Location:index.php');
