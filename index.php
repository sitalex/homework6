<?php
require_once 'db/db.php';
require_once 'classes/Factory.php';
$persons = Factory::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<a href="create_db.php">Создать БД</a>
<form action="info.php" method="post">
    Кто вы?<br>
    Я:<label>
        <select name="role">
            <option value="Студент">Студент</option>
            <option value="Преподаватель">Преподаватель</option>
            <option value="Администратор">Администратор</option>
        </select>
    </label><br>
    <button>Запомнить</button>
</form>
<table>
    <tr>
        <th>
            Роль
        </th>
        <th>
            ФИО
        </th>
        <th>
            Номер телефона
        </th>
        <th>
            @mail
        </th>
        <th>
        </th>
        <th>
        </th>
        <th>
        </th>
    </tr>
        <?php foreach ($persons as $person): ?>
            <tr>
                <td>
                    <?= $person->getRole() ?>
                </td>
                <td>
                    <?= $person->getFullName() ?>
                </td>
                <td>
                    <?= $person->getPhoneNumber() ?>
                </td>
                <td>
                    <?= $person->getEmail() ?>
                </td>
                <td>
                    <a href="infoPerson.php?id=<?= $person->getId() ?>&role=<?= $person->getRole() ?>">Подробнее</a>
                </td>
                <td>
                    <a href="editPerson.php?id=<?= $person->getId() ?>&role=<?= $person->getRole() ?>">Изменить</a>
                </td>
                <td>
                    <a href="deletePerson.php?id=<?= $person->getId() ?>">Удалить</a>
                </td>
            </tr>
        <?php endforeach; ?>
</table>

</body>
</html>
