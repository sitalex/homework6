<?php

$host       = 'localhost';
$dbUser     = 'root';
$dbPassword = '';
$dbName     = 'school';

try
{
    $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES "utf8"');
}
catch (Exception $ex)
{
    echo "Error connecting to db! " . $ex->getCode() . ' message: ' . $ex->getMessage();
    die();
}