<?php
require_once 'db/db.php';
require_once 'classes/Factory.php';

if (!empty($_POST['role']))
{
    $day = '';
    for ($i = 1; $i <= 7; $i++)
    {
        $b = "day$i";
        if (isset($_POST[$b]))
        {
            $day .= $_POST[$b] . ' ';
        }
    }

    Factory::update($_POST['id'], $_POST['role'], $_POST['fullName'], $_POST['phoneNumber'], $_POST['email'], $_POST['subject'], $_POST['average'], $day, $pdo);

} ?>
<script>alert('Изменено!')</script>
<meta http-equiv="refresh" content="0; url=index.php">