<?php
if (empty($_GET['id'] && $_GET['role']))
{
    header('Location:index.php');
}
require_once 'db/db.php';
require_once 'classes/Factory.php';
require_once 'classes/Student.php';
require_once 'classes/Teacher.php';
require_once 'classes/Admin.php';

if ($_GET['role'] == 'Студент')
{
    $person = Student::getPerson($_GET['id'], $pdo);
}
else if ($_GET['role'] == 'Преподаватель')
{
    $person = Teacher::getPerson($_GET['id'], $pdo);
}
else if ($_GET['role'] == 'Администратор')
{
    $person = Admin::getPerson($_GET['id'], $pdo);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?= $person->getVisitCardPerson() ?>
<a href="index.php">Вернуться</a>
</body>
</html>
