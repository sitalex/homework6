<?php
if (empty($_GET['id']))
{
    header('Location:index.php');
}

require_once 'db/db.php';
require_once 'classes/Factory.php';

Factory::delete($_GET['id'], $pdo);

header('Location:index.php');
