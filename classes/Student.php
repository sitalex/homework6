<?php


class Student extends Factory
{
    protected $average;
    protected $id;

    public function __construct($fullName, $phone, $email, $role, $average)
    {
        parent::__construct($role, $fullName, $phone, $email);
        $this->average = $average;
    }

    public static function getPerson($id, PDO $pdo)
    {
        $person = parent::getPerson($id, $pdo);

        return new self($person->getFullName(), $person->getPhoneNumber(), $person->getEmail(), $person->getRole(), $person->getAverage());

    }

    public function getVisitCardPerson()
    {
        $html = 'Успеваемость:' . $this->average . '<br>';

        return parent::getVisitCard($html);
    }

}