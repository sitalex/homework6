<?php


class Teacher extends Factory
{
    protected $subject;
    protected $id;

    public function __construct($fullName, $phone, $email, $role, $subject)
    {
        parent::__construct($role, $fullName, $phone, $email);
        $this->subject = $subject;
    }


    public static function getPerson($id, PDO $pdo)
    {
        $person = parent::getPerson($id, $pdo);

        return new self($person->getFullName(), $person->getPhoneNumber(), $person->getEmail(), $person->getRole(), $person->getSubject());

    }

    public function getVisitCardPerson()
    {
        $html = 'Предметы:' . $this->subject . '<br>';

        return parent::getVisitCard($html);
    }

}