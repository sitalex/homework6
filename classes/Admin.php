<?php


class Admin extends Factory
{

    protected $days;
    protected $id;


    public function __construct($fullName, $phone, $email, $role, $days)
    {
        parent::__construct($role, $fullName, $phone, $email);
        $this->days = $days;
    }

    public static function getPerson($id, PDO $pdo)
    {
        $person = parent::getPerson($id, $pdo);

        return new self($person->getFullName(), $person->getPhoneNumber(), $person->getEmail(), $person->getRole(), $person->getDays());

    }

    public function getVisitCardPerson()
    {
        $html = 'Дни работы:' . $this->days . '<br>';

        return parent::getVisitCard($html);
    }

}