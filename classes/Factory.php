<?php


class Factory
{
    protected $role;
    protected $fullName;
    protected $phoneNumber;
    protected $email;
    protected $id;

    public function __construct($role, $fullName, $phoneNumber, $email)
    {
        $this->role        = htmlspecialchars($role);
        $this->fullName    = htmlspecialchars($fullName);
        $this->phoneNumber = htmlspecialchars($phoneNumber);
        $this->email       = htmlspecialchars($email);
    }


    static public function getPerson($id, PDO $pdo)
    {
        try
        {
            $sql       = 'SELECT * FROM school WHERE id=:id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $personArr = $statement->fetchAll();
            $personArr = $personArr[0];
            $personObj = new self($personArr['role'], $personArr['full_name'], $personArr['phone'], $personArr['email']);
            $personObj->setId($personArr['id']);
            $personObj->setAverage($personArr['average_mark']);
            $personObj->setSubject($personArr['subject']);
            $personObj->setDays($personArr['working_day']);

            return $personObj;
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }

    }

    static public function delete($id, PDO $pdo)
    {
        $person = self::getPerson($id, $pdo);
        $person->destroy($pdo);
    }


    public function destroy(PDO $pdo)
    {
        try
        {
            $sql       = "DELETE FROM school WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function update($id, $role, $fullName, $phoneNumber, $email, $subject, $average, $day, PDO $pdo)
    {
        $person = new self($role, $fullName, $phoneNumber, $email);
        $person->setId($id);
        $person->setAverage($average);
        $person->setSubject($subject);
        $person->setDays($day);
        $person->edit($pdo);

    }

    static public function createPerson($role, $fullName, $phoneNumber, $email, $subject, $average, $day, PDO $pdo)
    {
        $person = new self($role, $fullName, $phoneNumber, $email);
        $person->setAverage($average);
        $person->setSubject($subject);
        $person->setDays($day);
        $person->create($pdo);

    }

    public function create(PDO $pdo)
    {
        try
        {
            $sql = 'INSERT INTO school SET
            role = :role,
            full_name = :fullName,
            phone = :phoneNumber,
            email = :email,
            subject = :subject,
            average_mark = :average,
            working_day = :days';

            $statement = $pdo->prepare($sql);
            $statement->bindValue(':role', $this->role);
            $statement->bindValue(':fullName', $this->fullName);
            $statement->bindValue(':phoneNumber', $this->phoneNumber);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':subject', $this->subject);
            $statement->bindValue(':average', $this->average);
            $statement->bindValue(':days', $this->days);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function all(PDO $pdo)
    {
        try
        {
            $sql        = "SELECT * FROM school";
            $person     = $pdo->query($sql);
            $personsArr = $person->fetchAll();
            $personObjs = [];
            foreach ($personsArr as $personArr)
            {
                $personObj = new self($personArr['role'], $personArr['full_name'], $personArr['phone'], $personArr['email']);
                $personObj->setId($personArr['id']);
                $personObj->setAverage($personArr['average_mark']);
                $personObj->setSubject($personArr['subject']);
                $personObj->setDays($personArr['working_day']);
                $personObjs[] = $personObj;
            }

            return $personObjs;
        }
        catch (Exception $exception)
        {
            die();
        }

    }

    public function getVisitCard($get)
    {
        $visitCard = '';
        $visitCard .= 'Фио:' . $this->fullName . '<br>';
        $visitCard .= 'Номер телефона:' . $this->phoneNumber . '<br>';
        $visitCard .= 'Почта:' . $this->email . '<br>';
        $visitCard .= 'Роль:' . $this->role . '<br>';
        $visitCard .= $get . '<br>';

        return $visitCard;
    }

    public function edit(PDO $pdo)
    {
        try
        {
            $sql = 'UPDATE school SET
            role = :role,
            full_name = :fullName,
            phone = :phoneNumber,
            email = :email,
            subject = :subject,
            average_mark = :average,
            working_day = :days
            WHERE id=:id';

            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->bindValue(':role', $this->role);
            $statement->bindValue(':fullName', $this->fullName);
            $statement->bindValue(':phoneNumber', $this->phoneNumber);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':subject', $this->subject);
            $statement->bindValue(':average', $this->average);
            $statement->bindValue(':days', $this->days);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }

    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = htmlspecialchars($subject);
    }

    public function getAverage()
    {
        return $this->average;
    }

    public function setAverage($average)
    {
        if (!empty($average))
        {
            $this->average = htmlspecialchars($average);
        }
        else
        {
            $this->average = 0;
        }
    }

    public function getDays()
    {
        return $this->days;
    }

    public function setDays($days)
    {
        $this->days = htmlspecialchars($days);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = htmlspecialchars($id);
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function getEmail()
    {
        return $this->email;
    }

}